import de.voidplus.leapmotion.*;
import processing.serial.*;
Serial port;

String sending;

LeapMotion leap;

void setup() {
  size(800, 500, P3D);
  background(255);

  leap = new LeapMotion(this);

  println("Available serial ports:");
  println(Serial.list());
  port = new Serial(this, Serial.list()[0], 9600);
}


void draw() {
  background(255);
  
  int fps = leap.getFrameRate();
  for (Hand hand : leap.getHands ()) {

    // ==================================================
    // 2. Hand

    int     handId             = hand.getId();
    PVector handPosition       = hand.getPosition();
    println("Hand Position X"+handPosition.x);
    println("Hand Position Y"+handPosition.y);
    println("Hand Position Z"+handPosition.z);
    println(hand.getGrabStrength());
    int m1 = int(map(handPosition.x, 0, width, 600, 2400));
    int m2 = int(map(handPosition.y, 0, height, 1900, 600));
    int m3 = int(map(handPosition.z, 15, 65, 1100, 2300));
    int m4 = int(map(hand.getGrabStrength(), 0, 1, 1500, 2400));

    PVector handStabilized     = hand.getStabilizedPosition();
    PVector handDirection      = hand.getDirection();
    PVector handDynamics       = hand.getDynamics();
    float   handRoll           = hand.getRoll();
    float   handPitch          = hand.getPitch();
    float   handYaw            = hand.getYaw();
    boolean handIsLeft         = hand.isLeft();
    boolean handIsRight        = hand.isRight();
    float   handGrab           = hand.getGrabStrength();
    float   handPinch          = hand.getPinchStrength();
    float   handTime           = hand.getTimeVisible();
    PVector spherePosition     = hand.getSpherePosition();
    float   sphereRadius       = hand.getSphereRadius();

    // Drawing Hand------------------------------------- 
    hand.draw();

    // Sending serial-------------------------------
    sending = nf(m1,4)+" "+nf(m2,4)+" "+nf(m3,4)+" "+nf(m4,4)+"\n";
    port.write(sending);//write this to the serial port.
  }
}
