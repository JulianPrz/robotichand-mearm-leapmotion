// Test envío serial con mapeo del ratón

import processing.serial.*;
Serial port;
String sending;
float wh;

void setup() {
  size(600, 600);
  
  println("Available serial ports:");
  println(Serial.list());
  port = new Serial(this, Serial.list()[0], 9600);
}

void draw() {
  background(255);
  fill(255,0,0);
  noStroke();
  
  ellipse(mouseX,mouseY,15,15);
  int n1 = int(map(mouseX, 0, width, 600, 2400));
  int n2 = int(map(mouseY, 0, height, 1900, 600));
  int n3 = int(map(wh, 0, 255, 1100, 2300));
  int n4 = int(map(mouseY, 0, height, 1500, 2400));

  sending = nf(n1,4)+" "+nf(n2,4)+" "+nf(n3,4)+" "+nf(n4,4)+"\n";
  
  port.write(sending);
}

void mouseWheel(MouseEvent event) {
  float e = event.getCount()*10;
  wh = wh + e;
  if (wh>=255) {
    wh = 255;
  }
  if (wh <= 0) {
    wh = 0;
  }
}
