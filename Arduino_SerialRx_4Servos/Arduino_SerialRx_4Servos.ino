#include <Servo.h>

Servo myservo1;
Servo myservo2;
Servo myservo3;
Servo myservo4;

char line[22]; //String con 30 caracteres

void setup() {
  Serial.begin (9600);
  myservo1.attach(11, 600, 2400);
  myservo2.attach(10, 600, 1900);
  myservo3.attach(9, 1100, 2300);
  myservo4.attach(6, 1500, 2400);
  while (!Serial) {
    ;
  }
}
void mueveRobot(int n1, int n2, int n3, int n4) {
  myservo1.writeMicroseconds(n1); //set servo position
  myservo2.writeMicroseconds(n2);
  myservo3.writeMicroseconds(n3);
  myservo4.writeMicroseconds(n4);
}

void loop() {
  int n1,n2,n3,n4;
  if (Serial.available()) {
    Serial.readStringUntil('\n').toCharArray(line, 22);
    //"0123 0123 0123 0123"
    n1 = atoi(line);
    n2 = atoi(line + 5);
    n3 = atoi(line + 10);
    n4 = atoi(line + 15);
  }
  if (n1 * n2 * n3 * n4 != 0)
    mueveRobot(n1, n2, n3, n4);
}
